Predicting sentiment from product reviews

The goal of this assignment is to explore logistic regression and feature engineering with existing GraphLab Create functions.

In this assignment, you will use product review data from Amazon.com to predict whether the sentiments about a product (from its reviews) are positive or negative. You will:

Use SFrames to do some feature engineering
Train a logistic regression model to predict the sentiment of product reviews.
Inspect the weights (coefficients) of a trained logistic regression model.
Make a prediction (both class and probability) of sentiment for a new product review.
Given the logistic regression weights, predictors and ground truth labels, write a function to compute the accuracy of the model.
Inspect the coefficients of the logistic regression model and interpret their meanings.
Compare multiple logistic regression models.
