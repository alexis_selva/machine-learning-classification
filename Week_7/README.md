Training Logistic Regression via Stochastic Gradient Ascent

The goal of this assignment is to implement a logistic regression classifier using stochastic gradient ascent. You will:

Extract features from Amazon product reviews.
Convert an SFrame into a NumPy array.
Write a function to compute the derivative of log likelihood function (with L2 penalty) with respect to a single coefficient.
Implement stochastic gradient ascent with L2 penalty
Compare convergence of stochastic gradient ascent with that of batch gradient ascent

