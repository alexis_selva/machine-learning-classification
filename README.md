These are the programming assignments relative to Machine Learning - Classification (3rd part):

* Assignment 1: predicting sentiment from product reviews
* Assignment 2: implementing logistic regression classifier with L2 regularization
* Assignment 3: identifying safe loans with decision trees and implementing binary decision trees
* Assignment 4: exploring various techniques for preventing overfitting in decision trees
* Assignment 5: exploring the use of boosting
* Assignment 6: understanding precision-recall in the context of classifiers
* Assignment 7: implementing a logistic regression classifier using stochastic gradient ascent

For more information, I invite you to have a look at https://www.coursera.org/learn/ml-classification